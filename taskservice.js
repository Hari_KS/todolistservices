var db = require('./DB_connection');
var mysql = require("mysql");
var data=[];

var getDetails = function () {
return new Promise(function(resolve , rejects){
    data=[];
    db.con.query('select * from user', (err,rows,fields) => {
        if(err) throw err
        for(var i=0; i<rows.length; i++){
            data.push(rows[i]);
        }
         //console.log('ddata',data);
         resolve(data);  
    });
  
});
}

var taskdetails = function(){
    return new Promise(function(resolve , rejects){
        data=[];
        db.con.query('select * from tasklist', (err,rows,fields) => {
            if(err) throw err
            for(var i=0; i<rows.length; i++){
                data.push(rows[i]);
            }
             //console.log('ddata',data);
             resolve(data);  
        });

    });
}


var gettaskdetails = function(userId){
// console.log("userId", userId);
    return new Promise(function(resolve , rejects){
    let sql = 'select * from tasklist where User_id =' + mysql.escape(userId);
        db.con.query(sql, (err,rows,fields) => {
            if(err) throw err;
            for(var i=0; i<rows.length; i++){
                console.log('dbdata = ',rows[i]);  
            }
            resolve(rows);
        });    
    });
}

var deleteDetails = function(taskId){
    return new Promise(function(resolve, rejects){
        console.log("deleteDeatils = "+taskId);
        let sql = 'delete  from tasklist where id =' + mysql.escape( taskId );
         db.con.query(sql, (err,rows,fields) => {
        if(err) throw err;
        resolve();
    }); 

});
}
var updatedetails = function(taskId){
    return new Promise(function (resolve,rejects){
        let sql = 'update tasklist set status = "progress" where id = ' + mysql.escape( taskId );
        db.con.query(sql, (err,rows,fields) => {
            if(err) throw err;
            resolve();
    });

});
}
var completedDetails = function(taskId){
    return new Promise(function (resolve,rejects){
        console.log("Taskcompletelist id ="+taskId);  
        let sql = 'update tasklist set status = "completed" where id = ' + mysql.escape( taskId );
        db.con.query(sql, (err,rows,fields) => {
            if(err) throw err;
            resolve();
    });
});
}

var storedata = function(task){
    return new Promise(function(resolve,reject){
        // console.log("store data = "+task);
        let sql="insert into tasklist(User_id,description,status)values(1,"+ mysql.escape(task.name)+",'created')";
        db.con.query(sql,(err,rows,fields)=>{
            if (err){
                throw err;
            } else{
               resolve();
            }
        });
    });
    
}



module.exports = { getDetails,  gettaskdetails, deleteDetails, updatedetails,taskdetails,completedDetails,storedata};

