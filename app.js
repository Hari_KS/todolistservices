
const express = require('express');
const app = express();
const DB = require('./DB_connection');
const mysql = require('mysql');
const taskService = require("./taskservice.js");
const cors =require('cors');

app.use(cors());
app.options('*',cors());

/*
* this function is used to get the all user details
*/
app.get('/getuserdetails',(req,res) => {
    taskService.getDetails().then(function(data){
        // console.log('value = ',data);
        res.send(data);
    });   
});


/*
* this function used to get taskdetails
*/
app.get('/gettaskdetails',(req,res) =>{
   taskService.taskdetails().then(function(data){
        //    console.log("taskdetails = ",data);
           res.send(data);
   });

});


/*
* this function is used to get the specific task details using taskid
* @params: id -> taskId
*/
app.get('/gettaskdetails/:id',(req,res) =>{
    let id = req.params.id;
    taskService.gettaskdetails(id).then(function(data){
        res.send(data);
    });
});

app.get("/delete/task_id/:id",(req,res) => {
    let id = req.params.id;
    console.log("deleteId = "+id);
    taskService.deleteDetails( id ).then( function(){
        res.send('data is deleted');
    });
});

app.get("/completeList",(req,res) => { 
    // console.log(req.query.changeto);
    taskService.completedDetails(req.query.id).then(function(){
        res.send('data is updated');
    });
});

app.get("/updatelist",(req,res) => {
    
    // console.log(req.query.changeto);
    taskService.updatedetails(req.query.id).then(function(){
        res.send('data is updated');
    });
});

app.get('/request/server',(req,res) => {
    let task = req.query;
    // console.log("incoming data = "+task.name);
    taskService.storedata(task).then(function(){
        res.send("success");
    }); 
    
});

app.listen(3002,()=>{
    console.log("server Listening on port 3002");
});

